var ArtilleryDefence = function () {
    //variables here

    this.spritesheet = new Image();
    this.canvas = document.getElementById('artillerydefence-canvas');
    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;
    this.context = this.canvas.getContext('2d');
    this.ENEMY_START_POSITION_X = 10000;
    this.gameIsWon = false;
    this.availableAllies = 20;

    this.board = document.getElementById("scoreboard")
    this.scoreboard = document.getElementById("score");
    this.score = 0;
    this.liveboard = document.getElementById("lives");
    this.lives = document.getElementById("live");
    this.live = 20;

    this.wintitle = document.getElementById("win");
    this.losetitle = document.getElementById("lose");
    this.win = false;
    this.lose = false;
    this.loseclose = false;

    //frames stuff 
    this.lastFrameTime = 0;
    this.deltaTime = 0;
    this.fps = 0;

    //Keys
    this.ArrowUp = 38;
    this.ArrowDown = 40;
    this.isPressingUp = false;
    this.isPressingDown = false;
    this.isPressingSpace = false;
    this.gunAngle = 0;
    this.spacebar = 32;

    // Pause Game
    this.P_KEY = 80,
        this.isPaused = false;
    this.pauseDuration = 0;
    this.pauseOverlay = document.getElementById('pauseOverlay');
    this.pauseMenu = document.getElementById('pauseMenu');

    //Constant
    this.LEFT = 1,
        this.RIGHT = 2,
        window.devicePixelRatio = 1;

    this.SHORT_DELAY = 50; // milliseconds

    // Track baselines...................................................
    this.spawnBaseline = 1;
    this.TRACK_1_BASELINE = 3200;
    this.TRACK_2_BASELINE = 3400;
    this.TRACK_3_BASELINE = 3600;

    //Sprite sheet cell
    this.TOP = 337;
    this.KNIFEALLYTOP = 282;
    this.DEADALLY_TOP = 442;
    this.DEADENEMY_TOP = 492;

    this.TANK_CELLS_WIDTH = 193;
    this.TANK_CELLS_HEIGHT = 111;

    this.BULLET_CELLS_WIDTH = 39;
    this.BULLET_CELLS_HEIGHT = 39;

    this.GUN_CELLS_WIDTH = 144;
    this.GUN_CELLS_HEIGHT = 14;

    this.NOGUNSOLDIER_CELLS_WIDTH = 32;
    this.SOLDIER_CELLS_HEIGHT = 48;
    this.RUN_LEFT_START = 0;
    this.RUN_LEFT_END = 12;

    this.ENEMY_CELLS_HEIGHT = 42;

    this.tankCells = [{ left: 9, top: 786, width: this.TANK_CELLS_WIDTH, height: this.TANK_CELLS_HEIGHT }];
    this.bulletCells = [{ left: 4, top: 1059, width: this.BULLET_CELLS_WIDTH, height: this.BULLET_CELLS_HEIGHT }];
    this.gunCells = [{ left: 23, top: 975, width: this.GUN_CELLS_WIDTH, height: this.GUN_CELLS_HEIGHT }];

    //the start and end position of each sprite

    this.NONGUNALLY_START = 0;
    this.NONGUNALLY_END = 11;

    this.WALKENEMY_START = 12;
    this.WALKENEMY_END = 23;

    this.KNIFEMAN_START = 24;
    this.KNIFEMAN_END = 35;

    //physics
    this.gravity = 150;

    this.KNIFEALLY_START = 36;
    this.KNIFEALLY_END = 47;

    this.DEADALLY_START = 48;
    this.DEADALLY_END = 65;

    this.DEADENEMY_START = 66;
    this.DEADENEMY_END = 83;

    this.soldierCells = [
        //walk ally sprite
        { left: 1312 + 2, top: this.TOP, width: this.NOGUNSOLDIER_CELLS_WIDTH, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 1280 + 2, top: this.TOP, width: 29, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 1246 + 2, top: this.TOP, width: 28, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 1211 + 2, top: this.TOP, width: this.NOGUNSOLDIER_CELLS_WIDTH, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 1175 + 2, top: this.TOP, width: this.NOGUNSOLDIER_CELLS_WIDTH, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 1140 + 2, top: this.TOP, width: 26, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 1112 + 2, top: this.TOP, width: 28, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 1082 + 2, top: this.TOP, width: 25, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 1051 + 2, top: this.TOP, width: this.NOGUNSOLDIER_CELLS_WIDTH, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 1016 + 2, top: this.TOP, width: this.NOGUNSOLDIER_CELLS_WIDTH, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 978 + 2, top: this.TOP, width: this.NOGUNSOLDIER_CELLS_WIDTH, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 941 + 2, top: this.TOP, width: this.NOGUNSOLDIER_CELLS_WIDTH, height: this.SOLDIER_CELLS_HEIGHT },
        //walk enemy sprite
        { left: 528, top: this.TOP, width: 26, height: this.ENEMY_CELLS_HEIGHT },
        { left: 559, top: this.TOP, width: 30, height: this.ENEMY_CELLS_HEIGHT },
        { left: 595, top: this.TOP, width: 28, height: this.ENEMY_CELLS_HEIGHT },
        { left: 629, top: this.TOP, width: 28, height: this.ENEMY_CELLS_HEIGHT },
        { left: 662, top: this.TOP, width: 30, height: this.ENEMY_CELLS_HEIGHT },
        { left: 699, top: this.TOP, width: 25, height: this.ENEMY_CELLS_HEIGHT },
        { left: 727, top: this.TOP, width: 26, height: this.ENEMY_CELLS_HEIGHT },
        { left: 758, top: this.TOP, width: 27, height: this.ENEMY_CELLS_HEIGHT },
        { left: 790, top: this.TOP, width: 27, height: this.ENEMY_CELLS_HEIGHT },
        { left: 824, top: this.TOP, width: 30, height: this.ENEMY_CELLS_HEIGHT },
        { left: 859, top: this.TOP, width: 31, height: this.ENEMY_CELLS_HEIGHT },
        { left: 898, top: this.TOP, width: 32, height: this.ENEMY_CELLS_HEIGHT },
        //knifeMan enemy sprite
        { left: 484, top: this.TOP, width: 31, height: this.ENEMY_CELLS_HEIGHT },
        { left: 452, top: this.TOP, width: 31, height: this.ENEMY_CELLS_HEIGHT },
        { left: 419, top: this.TOP, width: 32, height: this.ENEMY_CELLS_HEIGHT },
        { left: 385, top: this.TOP, width: 32, height: this.ENEMY_CELLS_HEIGHT },
        { left: 349, top: this.TOP, width: 31, height: this.ENEMY_CELLS_HEIGHT },
        { left: 313, top: this.TOP, width: 33, height: this.ENEMY_CELLS_HEIGHT },
        { left: 273, top: this.TOP, width: 35, height: this.ENEMY_CELLS_HEIGHT },
        { left: 231, top: this.TOP, width: 37, height: this.ENEMY_CELLS_HEIGHT },
        { left: 190, top: this.TOP, width: 37, height: this.ENEMY_CELLS_HEIGHT },
        { left: 140, top: this.TOP, width: 44, height: this.ENEMY_CELLS_HEIGHT },
        { left: 84, top: this.TOP, width: 49, height: this.ENEMY_CELLS_HEIGHT },
        { left: 27, top: this.TOP, width: 44, height: this.ENEMY_CELLS_HEIGHT },
        //knifeMan ally sprite 36
        { left: 32, top: this.KNIFEALLYTOP, width: this.NOGUNSOLDIER_CELLS_WIDTH, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 65, top: this.KNIFEALLYTOP, width: this.NOGUNSOLDIER_CELLS_WIDTH, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 97, top: this.KNIFEALLYTOP, width: this.NOGUNSOLDIER_CELLS_WIDTH, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 130, top: this.KNIFEALLYTOP, width: this.NOGUNSOLDIER_CELLS_WIDTH, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 165, top: this.KNIFEALLYTOP, width: this.NOGUNSOLDIER_CELLS_WIDTH, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 198, top: this.KNIFEALLYTOP, width: this.NOGUNSOLDIER_CELLS_WIDTH, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 240, top: this.KNIFEALLYTOP, width: 33, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 279, top: this.KNIFEALLYTOP, width: 36, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 320, top: this.KNIFEALLYTOP, width: 38, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 363, top: this.KNIFEALLYTOP, width: 42, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 418, top: this.KNIFEALLYTOP, width: 42, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 477, top: this.KNIFEALLYTOP, width: 42, height: this.SOLDIER_CELLS_HEIGHT },
        //the dead ally 48
        { left: 15, top: this.DEADALLY_TOP, width: 43, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 59, top: this.DEADALLY_TOP, width: 47, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 107, top: this.DEADALLY_TOP, width: 50, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 158, top: this.DEADALLY_TOP, width: 56, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 216, top: this.DEADALLY_TOP, width: 59, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 276, top: this.DEADALLY_TOP, width: 60, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 337, top: this.DEADALLY_TOP, width: 61, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 400, top: this.DEADALLY_TOP, width: 65, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 466, top: this.DEADALLY_TOP, width: 74, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 541, top: this.DEADALLY_TOP, width: 73, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 615, top: this.DEADALLY_TOP, width: 78, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 696, top: this.DEADALLY_TOP, width: 78, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 780, top: this.DEADALLY_TOP, width: 82, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 865, top: this.DEADALLY_TOP, width: 82, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 950, top: this.DEADALLY_TOP, width: 80, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 1035, top: this.DEADALLY_TOP, width: 82, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 1120, top: this.DEADALLY_TOP, width: 35, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 1156, top: this.DEADALLY_TOP, width: 37, height: this.SOLDIER_CELLS_HEIGHT },
        //the dead enemy 66
        { left: 5, top: this.DEADENEMY_TOP, width: 75, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 81, top: this.DEADENEMY_TOP, width: 74, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 156, top: this.DEADENEMY_TOP, width: 62, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 219, top: this.DEADENEMY_TOP, width: 65, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 285, top: this.DEADENEMY_TOP, width: 58, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 345, top: this.DEADENEMY_TOP, width: 59, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 405, top: this.DEADENEMY_TOP, width: 55, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 461, top: this.DEADENEMY_TOP, width: 51, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 513, top: this.DEADENEMY_TOP, width: 48, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 562, top: this.DEADENEMY_TOP, width: 42, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 613, top: this.DEADENEMY_TOP, width: 83, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 700, top: this.DEADENEMY_TOP, width: 83, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 789, top: this.DEADENEMY_TOP, width: 83, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 880, top: this.DEADENEMY_TOP, width: 83, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 972, top: this.DEADENEMY_TOP, width: 83, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 1059, top: this.DEADENEMY_TOP, width: 87, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 1150, top: this.DEADENEMY_TOP, width: 42, height: this.SOLDIER_CELLS_HEIGHT },
        { left: 1198, top: this.DEADENEMY_TOP, width: 42, height: this.SOLDIER_CELLS_HEIGHT },
        // win 84
        { left: 16, top: 605, width: 45, height: 62 },
        { left: 63, top: 605, width: 45, height: 62 },
        { left: 110, top: 605, width: 45, height: 62 },
        { left: 157, top: 605, width: 45, height: 62 },
    ];

    this.explosion_cells = [
        { left: 27, top: 1274, width: 91, height: 70 },
        { left: 127, top: 1274, width: 101, height: 70 },
        { left: 242, top: 1274, width: 101, height: 70 },
        { left: 347, top: 1266, width: 99, height: 70 },
        { left: 453, top: 1249, width: 104, height: 86 },
        { left: 18, top: 1352, width: 105, height: 101 },
        { left: 124, top: 1352, width: 103, height: 101 },
        { left: 230, top: 1352, width: 105, height: 101 },
        { left: 336, top: 1352, width: 105, height: 101 },
        { left: 444, top: 1350, width: 105, height: 101 },
        { left: 20, top: 1454, width: 108, height: 105 },
        { left: 129, top: 1456, width: 104, height: 103 },
        { left: 233, top: 1455, width: 102, height: 103 },
        { left: 338, top: 1454, width: 102, height: 103 },
        { left: 452, top: 1452, width: 102, height: 103 }
    ];

    this.winSoldierCells = [
        { left: 16, top: 605, width: 45, height: 62 },
        { left: 63, top: 605, width: 45, height: 62 },
        { left: 110, top: 605, width: 45, height: 62 },
        { left: 157, top: 605, width: 45, height: 62 },
    ];

    //Sprite Data
    // this.tankData = [{left: his.canvas.width*0.01, top: this.TRACK_2_BASELINE}];

    //Sprite
    this.tank = [];

    //Fade out
    this.TRANSPARENT = 1;

    //Loading screen elements
    this.loadingElement = document.getElementById('loading'),
        this.loadingTitleElement = document.getElementById('loading-title'),
        this.loadingAnimation = document.getElementById('loading-animation'),
        this.gameStarted = false,
        this.OPAQUE = 10.0,

        //Music
        this.musicElement = document.getElementById('music');
    this.musicCheckboxElement = document.getElementById('music-checkbox');
    this.musicElement.volume = 1;
    this.musicOn = this.musicCheckboxElement.checked;

    //Sounds
    this.audioSprites = document.getElementById('audio-sprites');
    this.soundCheckboxElement = document.getElementById('sound-checkbox');
    this.soundOn = this.soundCheckboxElement.checked;
    this.audioChannels = [
        { playing: false, audio: this.audioSprites, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null, },
        { playing: false, audio: null }
    ];

    this.audioSpriteCountdown = this.audioChannels - 1;
    this.graphicsReady = false;
    this.gameStarted = false;


    //Sound class
    this.battleEndSound = {
        position: 120.0, // seconds
        duration: 6000, // milliseconds
        volume: 1.0
    };

    this.openFire_Reload = {
        position: 130.35,
        duration: 3800,
        volume: 1.0
    };

    this.explosion = {
        position: 145,
        duration: 5000,
        volume: 1.0
    };

    this.ak47_1 = {
        position: 155,
        duration: 2000,
        volume: 1.0
    };

    this.ak47_2 = {
        position: 157,
        duration: 2000,
        volume: 1.0
    };

    this.m249_1 = {
        position: 164.8,
        duration: 1500,
        volume: 1.0
    };

    this.m249_2 = {
        position: 166.5,
        duration: 1500,
        volume: 1.0
    };

    this.glock18 = {
        position: 169.8,
        duration: 1500,
        volume: 1.0
    };

    this.knife = {
        position: 174.8,
        duration: 2000,
        volume: 1.0
    };

    this.hitTank_1 = {
        position: 179.5,
        duration: 1200,
        volume: 1.0
    };
    this.hitTank_2 = {
        position: 180.7,
        duration: 1200,
        volume: 1.0
    };
    this.soldierDie = {
        position: 185.0,
        duration: 2500,
        volume: 0.5
    };
    this.battleStart = {
        position: 190.0,
        duration: 1800,
        volume: 1.0
    };
    // this is not supposed to be done in the constructor. later soldiers will spawn overtime in the game. only the tank is ok
    /************* creation of game objects *************/
    // ground object
    this.ground = [new GameObject("Ground", "Ground", { x: 0, y: this.TRACK_2_BASELINE + 500 })];
    this.ground[0].setCollision(new Collision(this.ground, 1000 * this.canvas.width * 0.001, 70 * this.canvas.width * 0.001));

    // test create some soldiers to array
    this.enemySoldiers = [
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 10000, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 60, moveSpeed: 300 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 10000, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 60, moveSpeed: 300 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 10000, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 60, moveSpeed: 300 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 12000, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 60, moveSpeed: 300 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 12000, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 60, moveSpeed: 300 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 12000, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 100, moveSpeed: 300 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 9500, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 60, moveSpeed: 300 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 9500, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 60, moveSpeed: 300 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 9500, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 100, moveSpeed: 300 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 10500, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 60, moveSpeed: 300 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 10500, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 60, moveSpeed: 300 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 10500, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 100, moveSpeed: 300 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 11000, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 60, moveSpeed: 300 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 11000, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 60, moveSpeed: 300 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 11000, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 100, moveSpeed: 300 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 11500, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 60, moveSpeed: 400 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 11500, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 60, moveSpeed: 400 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 11500, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 60, moveSpeed: 400 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 12500, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 60, moveSpeed: 400 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 12500, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 100, moveSpeed: 400 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 12500, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 60, moveSpeed: 400 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 13000, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 100, moveSpeed: 400 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 13000, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 60, moveSpeed: 400 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 13000, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 60, moveSpeed: 400 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 10000, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 60, moveSpeed: 400 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 10000, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 100, moveSpeed: 400 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 10000, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 60, moveSpeed: 400 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 12000, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 60, moveSpeed: 400 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 12000, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 60, moveSpeed: 400 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 12000, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 100, moveSpeed: 400 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 12500, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 60, moveSpeed: 400 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 12500, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 60, moveSpeed: 400 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 12500, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 60, moveSpeed: 400 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 13000, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 60, moveSpeed: 400 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 13000, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 60, moveSpeed: 400 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 13000, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 100, moveSpeed: 400 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 13500, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 60, moveSpeed: 400 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 13500, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 60, moveSpeed: 400 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 13500, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 60, moveSpeed: 400 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 14000, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 100, moveSpeed: 500 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 14000, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 60, moveSpeed: 500 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 14000, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 60, moveSpeed: 500 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 14500, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 60, moveSpeed: 500 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 14500, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 60, moveSpeed: 500 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 14500, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 60, moveSpeed: 500 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 15000, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 60, moveSpeed: 500 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 15000, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 60, moveSpeed: 500 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 15000, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 60, moveSpeed: 500 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 15500, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 60, moveSpeed: 500 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 15500, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 60, moveSpeed: 500 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 15500, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 60, moveSpeed: 500 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 16000, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 60, moveSpeed: 500 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 16000, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 100, moveSpeed: 500 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 16000, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 60, moveSpeed: 500 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 16500, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 100, moveSpeed: 500 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 16500, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 60, moveSpeed: 500 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 16500, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 60, moveSpeed: 500 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 17000, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 60, moveSpeed: 600 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 17000, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 100, moveSpeed: 600 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 17000, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 60, moveSpeed: 600 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 17500, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 60, moveSpeed: 600 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 17500, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 60, moveSpeed: 600 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 17500, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 100, moveSpeed: 600 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 18000, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 60, moveSpeed: 600 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 18000, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 60, moveSpeed: 600 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 18000, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 60, moveSpeed: 600 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 18500, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 60, moveSpeed: 600 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 18500, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 60, moveSpeed: 600 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 18500, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 100, moveSpeed: 600 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 19000, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 60, moveSpeed: 600 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 19000, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 60, moveSpeed: 600 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 19000, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 60, moveSpeed: 600 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 19500, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 100, moveSpeed: 600 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 19500, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 60, moveSpeed: 600 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 20000, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 60, moveSpeed: 600 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 20000, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 60, moveSpeed: 600 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 20000, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 60, moveSpeed: 600 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 20500, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 60, moveSpeed: 600 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 20500, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 60, moveSpeed: 600 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 20500, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 60, moveSpeed: 600 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 20500, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 60, moveSpeed: 600 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 15500, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 60, moveSpeed: 700 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 16000, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 60, moveSpeed: 700 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 16000, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 100, moveSpeed: 700 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 16000, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 60, moveSpeed: 700 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 16500, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 100, moveSpeed: 700 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 16500, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 60, moveSpeed: 700 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 16500, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 60, moveSpeed: 700 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 17000, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 60, moveSpeed: 700 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 17000, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 100, moveSpeed: 700 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 17000, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 60, moveSpeed: 700 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 17500, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 60, moveSpeed: 700 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 17500, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 60, moveSpeed: 700 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 17500, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 100, moveSpeed: 700 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 18000, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 60, moveSpeed: 700 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 18000, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 60, moveSpeed: 700 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 18000, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 60, moveSpeed: 700 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 18500, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 60, moveSpeed: 700 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 18500, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 60, moveSpeed: 700 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 18500, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 100, moveSpeed: 700 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 19000, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 60, moveSpeed: 700 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 19000, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 60, moveSpeed: 700 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 19000, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 60, moveSpeed: 700 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 19500, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 100, moveSpeed: 700 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 19500, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 60, moveSpeed: 700 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 20000, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 60, moveSpeed: 700 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 20000, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 60, moveSpeed: 700 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 20000, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 60, moveSpeed: 700 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 20500, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 60, moveSpeed: 700 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 20500, y: this.TRACK_1_BASELINE } }, { health: 600, damage: 60, moveSpeed: 700 }),
        new Soldier({ label: "enemy", name: "Suicide Bomber", position: { x: 20500, y: this.TRACK_2_BASELINE } }, { health: 600, damage: 60, moveSpeed: 700 }),
        new Soldier({ label: "enemy", name: "Machine Gunner", position: { x: 20500, y: this.TRACK_3_BASELINE } }, { health: 600, damage: 60, moveSpeed: 700 }),
    ];
    this.allySoldiers = [


    ];

    this.soldiersArray = [
        this.enemySoldiers,
        this.allySoldiers,
    ];
    this.bulletsInGame = [];
    this.ammo = 5;
    this.explosionsInGame = [];
    // test create player tank as a game object
    this.playerTank = new GameObject("player", "Tank", { x: 0, y: this.TRACK_3_BASELINE - this.TANK_CELLS_WIDTH * 5 });

    //this.TankBullet = new Bullet({ label: "player", name: "bullet", position:{ x: 1400, y: 2000}}, {health: 1000, damage: 500, moveSpeed: 1500});
    this.TankGun = new Gun({ label: "player", name: "gun", position: { x: 0, y: 0 } }, { health: 0, damage: 0, moveSpeed: 100 });
    // 1550
    // 2720
    /************* test setting up of game objects *************/

    //set sprite for player tank
    this.playerTank.setSprites(new Sprite(this.playerTank, new SpriteSheetArtist(this.spritesheet, this.tankCells, 1)));

    // Set up soldier sprites, behaviours and collisions
    this.soldiersArray.forEach(soldierArray => {
        soldierArray.forEach(soldier => {
            // set up sprites
            soldier.setSprites(new Sprite(soldier, new SpriteSheetArtist(this.spritesheet, this.soldierCells, 15)));

            let sizeMultiplier = soldier.sprites.artist.sizeMultiplier;
            // setting up collisions
            soldier.setCollision(new Collision(soldier, this.NOGUNSOLDIER_CELLS_WIDTH * this.canvas.width * sizeMultiplier, this.SOLDIER_CELLS_HEIGHT * this.canvas.width * sizeMultiplier));

            // setting up behaviours
            if (soldier.getLabel() == "enemy") {
                soldier.setBehaviour(function () {
                    //artilleryDefence.context.fillRect(this.position.x * artilleryDefence.canvas.width * 0.0001, this.position.y * artilleryDefence.canvas.width * 0.0001, 32 * artilleryDefence.canvas.width * 0.001, 48 * artilleryDefence.canvas.width * 0.001);
                    // check if alive
                    if (this.health <= 0) {
                        this.die();
                    }

                    if (this.isAlive) {
                        // check if colliding with ally
                        let hitResults = this.collision.isColliding(artilleryDefence.allySoldiers);
                        if (hitResults) {
                            this.attack(hitResults[0]);
                        } else {
                            this.moveRight();
                        }
                    }
                });
            } else if (soldier.getLabel() == "ally") {
                soldier.setBehaviour(function () {
                    //artilleryDefence.context.fillRect(this.position.x * artilleryDefence.canvas.width * 0.0001, this.position.y * artilleryDefence.canvas.width * 0.0001, 32 * artilleryDefence.canvas.width * 0.001, 48 * artilleryDefence.canvas.width * 0.001);
                    // check if alive
                    if (!artilleryDefence.gameIsWon) {
                        if (this.health <= 0) {
                            this.die();
                        }

                        if (this.isAlive) {
                            // check if colliding with enemy
                            let hitResults = this.collision.isColliding(artilleryDefence.enemySoldiers);
                            if (hitResults) {
                                this.attack(hitResults[0]);
                            } else {
                                this.moveLeft();
                            }
                        }
                    } else {
                        this.celebrate();
                    }
                });
            }
        });
    });

    this.TankGun.setSprites(new Sprite(this.TankGun, new SpriteSheetArtist(this.spritesheet, this.gunCells, 1)));
}

ArtilleryDefence.prototype = {
    // methods here
    initializeImages: function () {
        this.spritesheet.src = "assets/sprites/spritesheet.png";
        this.loadingAnimation.src = 'assets/images/loading.gif';
        this.board.setAttribute("style", "margin-top:" + window.innerWidth * 0 + "em;");
        this.liveboard.setAttribute("style", "margin-top:" + window.innerWidth * 0.001 + "em;");
        this.board.setAttribute("style", "font-size:" + window.innerWidth * 0.0015 + "em;");
        this.liveboard.setAttribute("style", "font-size:" + window.innerWidth * 0.0015 + "em;");

        this.lives.setAttribute("style", "font-size:" + window.innerWidth * 0.0005 + "em;");
        this.scoreboard.setAttribute("style", "display:" + "inline-block");
        this.lives.setAttribute("style", "display:" + "inline-block");
        this.loadingElement.setAttribute("style", "width:" + window.innerWidth * 0.002 + "em;");
        this.loadingTitleElement.setAttribute("style", "width:" + window.innerWidth * 0.002 + "em;");
        this.loadingAnimation.setAttribute("style", "width:" + window.innerWidth * 0.008 + "em;");
        this.losetitle.setAttribute("style", "width:" + window.innerWidth * 0.008 + "em;");
        this.losetitle.setAttribute("style", "margin-top:" + window.innerWidth * 0.001 + "em;");
        this.wintitle.setAttribute("style", "width:" + window.innerWidth * 0.008 + "em;");
        this.wintitle.setAttribute("style", "margin-top:" + window.innerWidth * 0.001 + "em;");

        this.loadingAnimation.onload = function () {
            artilleryDefence.loadingGIF();  // Loading page
        };

        this.spritesheet.onload = function (e) {
            artilleryDefence.loadingGame();
        };
    },
    startGame: function () {
        // artilleryDefence.canvas.width = window.innerWidth;
        // artilleryDefence.canvas.height = window.innerHeight;

        requestAnimationFrame(function (now) {
            artilleryDefence.animate(now);
        });
        artilleryDefence.createAudioChannels();
        artilleryDefence.startMusic();
        artilleryDefence.playSound(artilleryDefence.battleStart);
    },
    animate: function (now) {
        if (!this.isPaused) {
            this.deltaTime = now - artilleryDefence.lastFrameTime;
            this.fps = 1000 / artilleryDefence.deltaTime;

            //console.log(artilleryDefence.fps);

            // render everything
            this.draw();

            // process bullets
            this.processBulletCollisions();

            // check collisions with explosion
            this.checkCollisionWithExplosions();

            this.lastFrameTime = now;

            requestAnimationFrame(function (now) {
                artilleryDefence.animate(now);
            });
        } else {
            // if paused
            this.lastFrameTime = now;
            requestAnimationFrame(function (now) {
                artilleryDefence.animate(now);
            });
            if(ArtilleryDefence.fps < 20){
                alert('Your Fps is too low.');
            }
        }
    },
    draw: function () {

        // clear the game screen
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        artilleryDefence.drawBackground();
        this.drawGun();
        artilleryDefence.drawTank();
        this.drawBulletsInGame();
        this.simulateSoldiers();
        this.animateExplosions();
    },
    drawBulletsInGame: function () {
        this.bulletsInGame.forEach(bullet => {
            bullet.sprites.draw(this.canvas, this.context);
            bullet.flyLeft();
        });
    },
    spawnNewBullet: function () {
        let gunLength = 1350;
        let startPositionX = (1550 + (Math.cos(this.gunAngle * Math.PI / 180) * gunLength));
        let startPositionY = 2980 - Math.sin(this.gunAngle * Math.PI / 180) * gunLength;
        let startVelocityY = Math.sin(this.gunAngle * Math.PI / 180) * 6500;
        console.log(-Math.sin(this.gunAngle * Math.PI / 180) * gunLength);
        let newBullet = new Bullet({ label: "player", name: "bullet", position: { x: startPositionX, y: startPositionY } }, { health: 1000, damage: 500, moveSpeed: 5000 }, -startVelocityY);
        newBullet.setSprites(new Sprite(newBullet, new SpriteSheetArtist(this.spritesheet, this.bulletCells, 1)));
        newBullet.setCollision(new Collision(newBullet, this.BULLET_CELLS_WIDTH * this.canvas.width * 0.001, this.BULLET_CELLS_HEIGHT * this.canvas.width * 0.001));
        // console.log(newBullet.position);
        this.bulletsInGame.push(newBullet);
    },

    drawGun: function () {
        if (this.isPressingUp && this.gunAngle <= 45) {
            this.gunAngle++;
        }
        if (this.isPressingDown && this.gunAngle >= -10) {
            this.gunAngle--;
        }

        this.context.translate(1520 * this.canvas.width * 0.0001, 3120 * this.canvas.width * 0.0001);
        this.context.rotate(-this.gunAngle * Math.PI / 180); // rotate 0° to -45°
        this.TankGun.sprites.draw(this.canvas, this.context);
        this.context.rotate(this.gunAngle * Math.PI / 180);
        this.context.translate(-(1520 * this.canvas.width * 0.0001), -(3120 * this.canvas.width * 0.0001));
    },

    simulateSoldiers: function () {

        this.soldiersArray.forEach(soldierArray => {
            soldierArray.forEach(soldier => {
                soldier.runBehaviour();
            });
        });
        this.lives.innerHTML = this.live;
        this.reduceHP();

    },
    processBulletCollisions: function () {
        for (let i = 0; i < this.bulletsInGame.length; i++) {
            if (this.bulletsInGame[i].collision.isColliding(this.ground)) {
                artilleryDefence.spawnExplosion(this.bulletsInGame[i].position);
                this.bulletsInGame.splice(i, 1);
            }
        }
    },
    animateExplosions: function () {
        this.explosionsInGame.forEach(explosion => {
            //this.context.fillRect(explosion.position.x * this.canvas.width * 0.0001, explosion.position.y * this.canvas.width * 0.0001, 100 * this.canvas.width * 0.001, 100 * this.canvas.width * 0.001);
            if (explosion.sprites.artist.cellIndex > 0) {
                explosion.collision.disable();
            }

            if (Math.floor(explosion.sprites.artist.cellIndex) == this.explosion_cells.length - 1) {
                this.explosionsInGame.shift();
            } else {
                explosion.sprites.animate(this.canvas, this.context, false);
            }
        });
    },
    checkCollisionWithExplosions: function () {
        this.enemySoldiers.forEach(enemy => {
            if (enemy.collision.isColliding(this.explosionsInGame) != false) {
                enemy.health -= 200;
            }
            document.getElementById("score").innerHTML = this.score;
        });
    },
    spawnExplosion: function (position) {
        let explosion = new GameObject("Explosion", "explosion", { x: position.x, y: position.y - 500 });
        explosion.setSprites(new Sprite(explosion, new SpriteSheetArtist(this.spritesheet, this.explosion_cells, 15)));
        explosion.setCollision(new Collision(explosion, 100 * this.canvas.width * 0.001, 100 * this.canvas.width * 0.001));
        this.explosionsInGame.push(explosion);
        artilleryDefence.playSound(artilleryDefence.explosion);
    },
    drawBackground: function () { // draw the background
        artilleryDefence.context.drawImage(this.spritesheet, 1582, 0, 1920, 1080, 0, 0, this.canvas.width, this.canvas.width * (1080 / 1920));
    },

    drawTank: function () {
        // artilleryDefence.context.drawImage(this.spritesheet, 9, 786, 193,111, 100,100, 193,111);
        // artilleryDefence.context.drawImage(this.spritesheet, 9, 786, 193,111, this.canvas.width*0.01,this.canvas.width * (1080/1920)*0.58, this.canvas.width*0.1, this.canvas.width * (1080/1920)*0.1);

        this.playerTank.sprites.draw(this.canvas, this.context);

        //artilleryDefence.context.drawImage(this.spritesheet, 9, 786, 193,111, 100,100, 193,111);
        //artilleryDefence.context.drawImage(this.spritesheet, 9, 786, 193,111, this.canvas.width*0.01,this.canvas.width * (1080/1920)*0.58, this.canvas.width*0.1, this.canvas.width * (1080/1920)*0.1);
    },
    startMusic: function () {
        var MUSIC_DELAY = 1000;
        artilleryDefence.musicElement.loop = true;
        setTimeout(function () {
            if (artilleryDefence.musicCheckboxElement.checked) {
                artilleryDefence.musicElement.play();
            }

            //    artilleryDefence.pollMusic();

        }, MUSIC_DELAY);
    },

    //sound sprite
    createAudioChannels: function () {
        var channel;

        for (var i = 0; i < this.audioChannels.length; ++i) {
            channel = this.audioChannels[i];

            if (i !== 0) {
                channel.audio = document.createElement('audio');
                channel.audio.addEventListener(
                    'loadeddata',  // event
                    this.soundLoaded,  // callback
                    false  // use captrue
                );

                channel.audio.src = this.audioSprites.currentSrc;
            }

            channel.audio.autobuffer = true;
        }
    },

    soundLoaded: function () {
        artilleryDefence.audioSpriteCountdown--;

        if (artilleryDefence.audioSpriteCountdown === 0) {
            if (!artilleryDefence.gameStarted && artilleryDefence.graphicsReady) {
                artilleryDefence.startGame();
            }
        }
    },

    soundSpriteLoaded: function () {
        var LOADING_SCREEN_TRANSITION_DURATION = 2000;

        this.graphicsReady = true;

        setTimeout(function () {
            if (!artilleryDefence.gameStarted && artilleryDefence.audioSpriteCountdown === 0) {
                artilleryDefence.startGame();
            }
        }, 1000);
    },

    getFirstAvailableAudioChannel: function () {
        for (var i = 0; i < this.audioChannels.length; ++i) {
            if (!this.audioChannels[i].playing) {
                return this.audioChannels[i];
            }
        }

        return null;
    },

    playSound: function (sound) {
        var channel,
            audio;

        if (this.soundOn) {
            channel = this.getFirstAvailableAudioChannel();
            if (!channel) {
                if (console) {
                    console.warn('All audio channels are busy. ' + 'Cannot play sound');
                }
            }
            else {
                audio = channel.audio;
                audio.volume = sound.volume;

                this.seekAudio(sound, audio);
                this.playAudio(audio, channel);

                setTimeout(function () {
                    channel.playing = false;
                    artilleryDefence.seekAudio(sound, audio);
                }, sound.duration);
            }
        }
    },

    seekAudio: function (sound, audio) {
        try {
            audio.pause();
            audio.currentTime = sound.position;
        }
        catch (e) {
            if (console) {
                console.error('Cannot seek audio');
            }
        }
    },

    playAudio: function (audio, channel) {
        try {
            audio.play();
            channel.playing = true;
        }
        catch (e) {
            if (console) {
                console.error('Cannot play audio');
            }
        }
    },

    fadeOutElements: function () {
        var args = arguments,
            fadeDuration = args[args.length - 1]; // Last argument

        for (var i = 0; i < args.length - 1; ++i) {
            args[i].style.opacity = this.TRANSPARENT;
        }

        setTimeout(function () {
            for (var i = 0; i < args.length - 1; ++i) {
                args[i].style.display = 'none';
            }
        }, fadeDuration);
    },

    fadeInElements: function () {
        var args = arguments;

        for (var i = 0; i < args.length; ++i) {
            args[i].style.display = 'block';
        }

        setTimeout(function () {
            for (var i = 0; i < args.length; ++i) {
                args[i].style.opacity = artilleryDefence.OPAQUE;
            }
        }, this.SHORT_DELAY);
    },

    loadingGIF: function () {  // loading animation with fadeIn
        if (!this.gameStarted) {
            this.fadeInElements(this.loadingAnimation, this.loadingTitleElement);
        }
    },

    loadingGame: function () {  // Loading Screen function
        var Loading_Screen_Transition_Duration = 5000; // Loading time

        this.fadeOutElements(this.loadingElement, Loading_Screen_Transition_Duration);

        setTimeout(function () {
            artilleryDefence.startGame();
            artilleryDefence.gameStarted = true;
            this.graphicsReady = true;
        }, Loading_Screen_Transition_Duration);
    },

    reduceHP: function () {
        this.enemySoldiers.forEach(enemy => {
            if (enemy.position.x < -400) {
                this.live--;
                if(this.live <= 0){
                    this.losetitle.setAttribute("style", "display:" + "block");
                    this.lose = true;
                }
                enemy.position.x = 12000;
                if(!this.lose && this.loseclose == false){
                    artilleryDefence.playSound(artilleryDefence.battleEndSound);
                    this.loseclose = true;
                }
            }
        });
        if(this.live <= 0){
            this.live = 0;
        }
    },

}

var artilleryDefence = new ArtilleryDefence();

artilleryDefence.initializeImages();




/*********** functions ************/




window.onblur = function (e) {
    pauseGame();
    artilleryDefence.musicElement.pause();
};

function pauseGame() {

    artilleryDefence.isPaused = true;

}
function resumeGame() {

    artilleryDefence.isPaused = false;

}

artilleryDefence.musicCheckboxElement.addEventListener(
    'change',

    function (e) {
        artilleryDefence.musicOn = artilleryDefence.musicCheckboxElement.checked;

        if (artilleryDefence.musicOn) {
            artilleryDefence.musicElement.play();
            artilleryDefence.musicElement.volume = 1;
        }
        else {
            artilleryDefence.musicElement.pause();
            artilleryDefence.musicElement.volume = 0;
        }
    }
);

artilleryDefence.soundCheckboxElement.addEventListener('change',

    function (e) {
        artilleryDefence.soundOn = artilleryDefence.soundCheckboxElement.checked;

        if (artilleryDefence.soundOn) {
            artilleryDefence.audioSprites.volume = 1;
        }
        else {
            artilleryDefence.audioSprites.volume = 0;
        }
    }
);

window.addEventListener('keydown', function (e) { // pause game by button P
    var key = e.keyCode;
    // var downArrow = document.getElementById("downArray").src = "./assets/images/downArrow.png";
    // var upArrow = document.getElementById("upArrow").src = "./assets/images/upArrow.png";

    var menu = document.getElementById("arena");
    if (key === artilleryDefence.P_KEY && !artilleryDefence.isPaused) {
        if (menu.style.display == "none") {
            menu.style.display = "block";
        }
        pauseGame();
        artilleryDefence.musicElement.pause();
        artilleryDefence.audioSprites.volume = 0;
    } else {
        if (menu.style.display == "block") {
            menu.style.display = "none";
        }
        resumeGame();
        artilleryDefence.musicElement.play();
        artilleryDefence.audioSprites.volume = 1;
    }
    if (key === artilleryDefence.ArrowUp) {
        artilleryDefence.isPressingUp = true;
    }
    if (key === artilleryDefence.ArrowDown) {
        artilleryDefence.isPressingDown = true;
    }
    if (key === artilleryDefence.spacebar) {
        if (artilleryDefence.ammo > 0) {
            artilleryDefence.spawnNewBullet();
            artilleryDefence.playSound(artilleryDefence.openFire_Reload);
            artilleryDefence.ammo --;
        } 
    }
});
window.addEventListener('keyup', function (e) { // pause game by button P
    var key = e.keyCode;
    // var downArrow = document.getElementById("downArray").src = "./assets/images/downArrow.png";
    // var upArrow = document.getElementById("upArrow").src = "./assets/images/upArrow.png";

    if (key === artilleryDefence.ArrowUp) {
        artilleryDefence.isPressingUp = false;
    }
    if (key === artilleryDefence.ArrowDown) {
        artilleryDefence.isPressingDown = false;
    }
});

//  window.addEventListener("resize", function(e){
//     // artilleryDefence.canvas.width = window.innerWidth;
//     // artilleryDefence.canvas.height = window.innerHeight;
//     resizeCanvas();    
// });

function resizeCanvas() {
    artilleryDefence.canvas.width = window.innerWidth;
    artilleryDefence.canvas.height = window.innerHeight;
    // artilleryDefence.canvas.mozImageSmoothingEnabled = true; //Better graphic for pixel art
    // artilleryDefence.canvas.msImageSmoothingEnabled = true;
    // artilleryDefence.canvas.imageSmoothingEnabled = true;
}

function goDown() {
    artilleryDefence.isPressingDown = true;
}
function stopGoingDown() {
    artilleryDefence.isPressingDown = false;
}

function goUp() {

    console.log("test");
    artilleryDefence.isPressingUp = true;
}

function stopGoingUp() {
    artilleryDefence.isPressingUp = false;
}

function shoot() {
    if (artilleryDefence.ammo > 0) {
        artilleryDefence.spawnNewBullet();
        artilleryDefence.playSound(artilleryDefence.openFire_Reload);
        artilleryDefence.ammo --;
    } 
}
function spawnNewAlly() {
    if (artilleryDefence.availableAllies > 0) {
        let baseline = 0;

        if (artilleryDefence.spawnBaseline > 3) {
            artilleryDefence.spawnBaseline = 1;
        }

        if (artilleryDefence.spawnBaseline == 1) {
            baseline = artilleryDefence.TRACK_1_BASELINE;
        } else if (artilleryDefence.spawnBaseline == 2) {
            baseline = artilleryDefence.TRACK_2_BASELINE;
        } else if (artilleryDefence.spawnBaseline == 3) {
            baseline = artilleryDefence.TRACK_3_BASELINE;
        }

        artilleryDefence.spawnBaseline++;

        let newSoldier = new Soldier({ label: "ally", name: "Hand Gunner", position: { x: -400, y: baseline } }, { health: 100, damage: 70, moveSpeed: 430 });
        newSoldier.setSprites(new Sprite(newSoldier, new SpriteSheetArtist(artilleryDefence.spritesheet, artilleryDefence.soldierCells, 15)));
        newSoldier.setCollision(new Collision(newSoldier, artilleryDefence.NOGUNSOLDIER_CELLS_WIDTH * artilleryDefence.canvas.width * 0.001, artilleryDefence.SOLDIER_CELLS_HEIGHT * artilleryDefence.canvas.width * 0.001));
        newSoldier.setBehaviour(function () {
            //artilleryDefence.context.fillRect(this.position.x * artilleryDefence.canvas.width * 0.0001, this.position.y * artilleryDefence.canvas.width * 0.0001, 32 * artilleryDefence.canvas.width * 0.001, 48 * artilleryDefence.canvas.width * 0.001);
            // check if alive
            if (!artilleryDefence.gameIsWon) {
                if (this.health <= 0) {
                    this.die();
                }

                if (this.isAlive) {
                    // check if colliding with enemy
                    let hitResults = this.collision.isColliding(artilleryDefence.enemySoldiers);
                    if (hitResults) {
                        this.attack(hitResults[0]);
                    } else {
                        this.moveLeft();
                    }
                }
            } else {
                this.celebrate();
            }
        });
        artilleryDefence.allySoldiers.push(newSoldier);
        artilleryDefence.availableAllies--;
    }
}