var Gun = function (objectDetails, stats, sprites) {

    GameObject.call(this, objectDetails.label, objectDetails.name, objectDetails.position);

    this.health = stats.health;
    this.damage = stats.damage;
    this.moveSpeed = stats.moveSpeed;

    // optional behaviour of object
    this.behaviour = null;

    //optional sprites
    if (sprites != null) {
        this.setSprites(sprites);
    }
}

// inherit GameObject Methods
Gun.prototype = new GameObject();

Gun.prototype.runBehaviour = function () {
    if (this.behaviour != null) {
        this.behaviour();
    } else {
        console.log(this.getName() + " has no behaviour method.");
    }
}

Gun.prototype.setBehaviour = function (behaviour) {
    this.behaviour = behaviour;
}