var Collision = function (gameObject, collisionWidth, collisionHeight) {
    this.gameObject = gameObject;
    this.width = collisionWidth;
    this.height = collisionHeight;
    this.enabled = true;
}

Collision.prototype = {
    setWidth: function (width) {
        this.width = width;
    },
    disable: function () {
        this.enabled = false;
    },
    enable: function () {
        this.enabled = true;
    },
    setHeight: function (height) {
        this.height = height;
    },
    isColliding: function (objectsToBeCheckedWith) {
        // returnedObjectArray is the array that 
        let returnedObjectArray = [];

        // topLeft          topRight      
        //  _________________
        // |                 |
        // |                 |
        // |                 |  
        // |_________________|
        //
        // bottomLeft       bottomRight

        let positionMultiplier = artilleryDefence.canvas.width * 0.0001;

        let topLeft = { x: this.gameObject.position.x * positionMultiplier, y: this.gameObject.position.y * positionMultiplier };
        let topRight = { x: this.gameObject.position.x * positionMultiplier + this.width, y: this.gameObject.position.y * positionMultiplier };
        let bottomLeft = { x: this.gameObject.position.x * positionMultiplier, y: this.gameObject.position.y * positionMultiplier + this.height };
        let bottomRight = { x: this.gameObject.position.x * positionMultiplier + this.width, y: this.gameObject.position.y * positionMultiplier + this.height };

        // check if this object is colliding with any objects from the objects array passed in
        objectsToBeCheckedWith.forEach(gameObject => {
            //check if collision is enabled
            if (gameObject.collision.enabled) {
                let gameObjectTL = { x: gameObject.position.x * positionMultiplier, y: gameObject.position.y * positionMultiplier };
                let gameObjectTR = { x: gameObject.position.x * positionMultiplier + gameObject.collision.width, y: gameObject.position.y * positionMultiplier };
                let gameObjectBL = { x: gameObject.position.x * positionMultiplier, y: gameObject.position.y * positionMultiplier + gameObject.collision.height };
                let gameObjectBR = { x: gameObject.position.x * positionMultiplier + gameObject.collision.width, y: gameObject.position.y * positionMultiplier + gameObject.collision.height };

                //check all four corners
                if (this.gameObject.id != gameObject.id) {
                    if (topLeft.x >= gameObjectTL.x && topLeft.x <= gameObjectTR.x && topLeft.y >= gameObjectTL.y && topLeft.y <= gameObjectBL.y) {
                        returnedObjectArray.push(gameObject);
                    } else if (topRight.x >= gameObjectTL.x && topRight.x <= gameObjectTR.x && topRight.y >= gameObjectTL.y && topRight.y <= gameObjectBL.y) {
                        returnedObjectArray.push(gameObject);
                    } else if (bottomLeft.x >= gameObjectTL.x && bottomLeft.x <= gameObjectTR.x && bottomLeft.y >= gameObjectTL.y && bottomLeft.y <= gameObjectBL.y) {
                        returnedObjectArray.push(gameObject);
                    } else if (bottomRight.x >= gameObjectTL.x && bottomRight.x <= gameObjectTR.x && bottomRight.y >= gameObjectTL.y && bottomRight.y <= gameObjectBL.y) {
                        returnedObjectArray.push(gameObject);
                    }
                }
            }
        });

        // if not colliding then return false
        if (returnedObjectArray.length != 0) {
            return returnedObjectArray;
        } else {
            return false;
        }
        // if colliding then set the returnedObject parameter to be the object colliding with and return true 
    }
}