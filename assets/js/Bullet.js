// example of new Soldier = new Soldier({label: "enemy", name: "Machine Gunner", position: {x: 0, y: 0}}, {health: 100, damage: 10, moveSpeed: 10})
var Bullet = function (objectDetails, stats, startVelocityY, arrayIndex) {

    GameObject.call(this, objectDetails.label, objectDetails.name, objectDetails.position);

    this.health = stats.health;
    this.damage = stats.damage;
    this.moveSpeed = stats.moveSpeed;
    this.velocityY = startVelocityY;
    this.arrayIndex = arrayIndex;

    // optional behaviour of object
    this.behaviour = null;
}

// inherit GameObject Methods
Bullet.prototype = new GameObject();

Bullet.prototype.runBehaviour = function () {
    if (this.behaviour != null) {
        this.behaviour();
    } else {
        console.log(this.getName() + " has no behaviour method.");
    }
}

Bullet.prototype.setBehaviour = function (behaviour) {
    this.behaviour = behaviour;
}

Bullet.prototype.flyLeft = function () {
    //console.log((ArtilleryDefence.deltaTime() / 1000));

    // calculate new velocity Y
    this.velocityY += artilleryDefence.gravity;

    this.position.x = this.position.x + (this.moveSpeed * (artilleryDefence.deltaTime / 1000));
    this.position.y = this.position.y + (this.velocityY * (artilleryDefence.deltaTime / 1000));
    // console.log(this.position.x);
    // console.log(this.position.y);
    //console.log(this.id + " is moving left");
}