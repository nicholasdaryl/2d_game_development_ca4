//Sprites Artists, draw sprites with a draw (sprite,context) method

SpriteSheetArtist = function (spritesheet, cells, animationRate) {
    this.cells = cells;
    this.spritesheet = spritesheet;
    this.sizeMultiplier = 0.001;
    this.animationRate = animationRate;
    this.cellIndex = 0;

};

SpriteSheetArtist.prototype = {
    //Draw sprites into canvas
    draw: function (sprite, context, canvas) {
        var cell = this.cells[this.cellIndex];
        let widthOnCanvas = cell.width * canvas.width * this.sizeMultiplier;
        let heightOnCanvas = cell.height * canvas.width * this.sizeMultiplier;
        let positionMultiplier = canvas.width * 0.0001;

        context.drawImage(this.spritesheet, cell.left, cell.top, cell.width, cell.height, sprite.gameObject.position.x * positionMultiplier, sprite.gameObject.position.y * positionMultiplier, widthOnCanvas, heightOnCanvas);
    },

    animate: function (sprite, context, canvas) {
        this.cellIndex += artilleryDefence.deltaTime / 1000 * this.animationRate;

        if (Math.floor(this.cellIndex) > this.cells.length - 1) {
            this.cellIndex = 0;
        }

        let widthOnCanvas = this.cells[Math.floor(this.cellIndex)].width * canvas.width * this.sizeMultiplier;
        let heightOnCanvas = this.cells[Math.floor(this.cellIndex)].height * canvas.width * this.sizeMultiplier;
        let positionMultiplier = canvas.width * 0.0001;

        //console.log(Math.floor(this.cellIndex));
        context.drawImage(this.spritesheet, this.cells[Math.floor(this.cellIndex)].left, this.cells[Math.floor(this.cellIndex)].top, this.cells[Math.floor(this.cellIndex)].width, this.cells[Math.floor(this.cellIndex)].height, sprite.gameObject.position.x * positionMultiplier, sprite.gameObject.position.y * positionMultiplier, widthOnCanvas, heightOnCanvas);

        if (Math.floor(this.cellIndex) > this.cells.length - 1) {
            this.cellIndex = 0;
        }
    },
    animateFromTo: function (sprite, startIndex, endIndex, context, canvas, loop) {
        this.cellIndex += artilleryDefence.deltaTime / 1000 * this.animationRate;

        if (startIndex + Math.floor(this.cellIndex) > endIndex) {
            if (loop) {
                this.cellIndex = 0;
            } else {
                this.cellIndex = endIndex - startIndex;
            }
        }

        let widthOnCanvas = this.cells[startIndex + Math.floor(this.cellIndex)].width * canvas.width * this.sizeMultiplier;
        let heightOnCanvas = this.cells[startIndex + Math.floor(this.cellIndex)].height * canvas.width * this.sizeMultiplier;
        let positionMultiplier = canvas.width * 0.0001;

        // console.log(this.cells[Math.floor(this.cellIndex)].width);
        context.drawImage(this.spritesheet, this.cells[startIndex + Math.floor(this.cellIndex)].left, this.cells[startIndex + Math.floor(this.cellIndex)].top, this.cells[startIndex + Math.floor(this.cellIndex)].width, this.cells[startIndex + Math.floor(this.cellIndex)].height, sprite.gameObject.position.x * positionMultiplier, sprite.gameObject.position.y * positionMultiplier, widthOnCanvas, heightOnCanvas);

        if (startIndex + Math.floor(this.cellIndex) > endIndex) {
            if (loop) {
                this.cellIndex = 0;
            } else {
                this.cellIndex = endIndex - startIndex;
            }
        }
    },
    //when in the last cell, return to the first one, in order to draw a set of images
    advance: function () {
        if (this.cellIndex === this.cells.length - 1) {
            this.cellIndex = 0;
        }
        else {
            this.cellIndex++;
        }
    },
};

//Sprite constructor
var Sprite = function (gameObject, artist) {
    this.artist = artist;
    this.gameObject = gameObject;
    this.opacity = 1.0;
    this.visible = true;
};

//Sprite methods
Sprite.prototype = {
    draw: function (canvas, context) {
        context.save();

        //Calls to save() and restore() make the globalAlpha setting tempory
        context.globalAlpha = this.opacity;

        if (this.artist && this.visible) {
            this.artist.draw(this, context, canvas);
        }

        context.restore();
    },
    animate: function (canvas, context) {
        context.save();

        //Calls to save() and restore() make the globalAlpha setting tempory
        context.globalAlpha = this.opacity;

        if (this.artist && this.visible) {
            this.artist.animate(this, context, canvas);
        }

        context.restore();
    },
    animateFromTo: function (startIndex, endIndex, canvas, context, loop) {
        context.save();

        //Calls to save() and restore() make the globalAlpha setting tempory
        context.globalAlpha = this.opacity;

        if (this.artist && this.visible) {
            this.artist.animateFromTo(this, startIndex, endIndex, context, canvas, loop);
        }

        context.restore();
    },
};

