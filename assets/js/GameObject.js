// example of new game object = new GameObject('Enemy', 'John Cena', {x: 0, y: 0})
var GameObject = function (label, name, position) {
    this.id = GameObject.id;
    this.label    = label;
    this.name     = name;
    this.position = position;
    // every object has an optional collisionData
    this.collision = null;
    // optional sprite
    this.sprites = null;

    GameObject.id++;
}

GameObject.id = 0;

GameObject.prototype = {
    getName: function () {
        return this.name;
    },
    getLabel: function () {
        return this.label;
    },
    getPosition: function () {
        return this.position;
    },
    setPosition: function (x, y) {
        this.position.x = x;
        this.position.y = y;
    },
    setSprites: function(sprites){
        this.sprites = sprites;
    },
    setCollision: function (collision) {
        this.collision = collision;
    }
}