function changeVolumenIcon(){
    var volumeText = document.getElementById("volumeTrigger");
    if(volumeText.innerHTML == "on"){
        document.getElementById("volume-on").src="./assets/images/volume_off.png";
        volumeText.innerHTML = "off";
        document.getElementById("music").volume = 0;
    }
    else if(volumeText.innerHTML == "off"){
        document.getElementById("volume-on").src="./assets/images/volume_on.png";
        volumeText.innerHTML = "on";
        document.getElementById("music").volume = 0.5;
    }
}

window.onload = function() {
    document.getElementById("music").play();
    document.getElementById("music").volume = 0.4;
    document.getElementById("music").loop = true;
    document.getElementById("menu").setAttribute("style", "font-size:" + window.innerWidth*0.002 + "em;");
    document.getElementById("helptext").setAttribute("style", "font-size:" + window.innerWidth*0.002 + "em;");
}

window.addEventListener("resize" ,function(e){
    document.getElementById("menu").setAttribute("style", "font-size:" + window.innerWidth*0.002 + "em;");
});

function changeDifficulty(){ 
    var difficulty = document.getElementById("changeLevelText");

    if(difficulty.innerHTML == "Easy"){
        difficulty.innerHTML = "Normal";
        difficulty.style.backgroundColor = "rgb(255,140,0,0.7)";
        localStorage.setItem("level", 2);
        localStorage.setItem("difficulty", "Normal");
    }
    else if(difficulty.innerHTML == "Normal"){
        difficulty.innerHTML = "Hard";
        difficulty.style.backgroundColor = "rgb(255,69,0,0.7)";
        localStorage.setItem("level", 3);
        localStorage.setItem("difficulty", "Hard");
    }
    else if(difficulty.innerHTML == "Hard"){
        difficulty.innerHTML = "Insane";
        difficulty.style.backgroundColor = "rgb(255,0,0, 0.8)";
        localStorage.setItem("level", 4);
        localStorage.setItem("difficulty", "Insane");
    } 

    else if(difficulty.innerHTML == "Insane"){
        difficulty.innerHTML = "Easy";
        difficulty.style.backgroundColor = "rgb(173,255,47,0.8)";
        localStorage.setItem("level", 1);
        localStorage.setItem("difficulty", "Easy");
    } 
}

function showOption(){
    var option = document.getElementById("option");
    if(option.style.display == "none"){
        option.style.display = "block";
    }
}

function closeOption(){
    option.style.display = "none";
}

function showScore(){
    var score = document.getElementById("menuscore");
    if(score.style.display == "none"){
        score.style.display = "block";
    }
}

function closeScore(){
    var score = document.getElementById("menuscore");
    score.style.display = "none";
}

function showHelp(){
    var help = document.getElementById("help");
    if(help.style.display == "none"){
        help.style.display = "block";
    }
}

function closeHelp(){
    help.style.display = "none";
}