// example of new Soldier = new Soldier({label: "enemy", name: "Machine Gunner", position: {x: 0, y: 0}}, {health: 100, damage: 10, moveSpeed: 10})
var Soldier = function (objectDetails, stats, sprites) {

    GameObject.call(this, objectDetails.label, objectDetails.name, objectDetails.position);

    this.health = stats.health;
    this.damage = stats.damage;
    this.moveSpeed = stats.moveSpeed;
    this.isAlive = true;
    this.isDamaging = false;

    // optional behaviour of object
    this.behaviour = null;

    //optional sprites
    if (sprites != null) {
        this.setSprites(sprites);
    }
}

// inherit GameObject Methods
Soldier.prototype = new GameObject();

Soldier.prototype.runBehaviour = function () {
    if (this.behaviour != null) {
        this.behaviour();
    } else {
        console.log(this.getName() + " has no behaviour method.");
    }
}

Soldier.prototype.setBehaviour = function (behaviour) {
    this.behaviour = behaviour;
}

Soldier.prototype.moveLeft = function () {
    //console.log((ArtilleryDefence.deltaTime() / 1000));
    this.sprites.animateFromTo(artilleryDefence.NONGUNALLY_START, artilleryDefence.NONGUNALLY_END, artilleryDefence.canvas, artilleryDefence.context, true);
    this.position.x = this.position.x + (this.moveSpeed * (artilleryDefence.deltaTime / 1000));
    //console.log(this.id + " is moving left");
}

Soldier.prototype.moveRight = function () {
    this.sprites.animateFromTo(artilleryDefence.WALKENEMY_START, artilleryDefence.WALKENEMY_END, artilleryDefence.canvas, artilleryDefence.context, true);
    this.position.x = this.position.x - (this.moveSpeed * (artilleryDefence.deltaTime / 1000));
    //console.log("is moving right");
}

Soldier.prototype.attack = function (enemy) {
    //console.log(this.sprites.artist.cellIndex);
    // animate attack 
    if (this.label == "enemy") {
        // attack at the end of the the animation for knife
        if ((24 + Math.floor(this.sprites.artist.cellIndex)) == 33) {
            if (!this.isDamaging) {
                this.isDamaging = true;
                enemy.health -= this.damage;
                artilleryDefence.playSound(artilleryDefence.knife);
                //console.log(enemy.health);
            }
        } else {
            this.isDamaging = false;
        }
        this.sprites.animateFromTo(24, 35, artilleryDefence.canvas, artilleryDefence.context, true);
    } else if (this.label == "ally") {
        // attack at the end of the the animation for knife
        if ((artilleryDefence.NONGUNALLY_START + Math.floor(this.sprites.artist.cellIndex)) == artilleryDefence.NONGUNALLY_START + 5) {
            if (!this.isDamaging) {
                this.isDamaging = true;
                enemy.health -= this.damage;
                //console.log(enemy.health);
            }
        } else {
            this.isDamaging = false;
        }
        this.sprites.animateFromTo(36, 47, artilleryDefence.canvas, artilleryDefence.context, true);
    }
    // when at the frame where it hit
    // decrease enemy health by this soldiers damage

    // if (this.sprite.artist.cellIndex == artilleryDefence.KNIFEENEMY_END)
    // enemy.health -= this.damage;

}

Soldier.prototype.die = function () {
    if (this.isAlive) {
        artilleryDefence.playSound(artilleryDefence.soldierDie);
        this.collision.disable();
        this.sprites.artist.cellIndex = 0;
        if(this.label == "enemy"){
            artilleryDefence.score++;
            if(artilleryDefence.score == 111){
                artilleryDefence.playSound(artilleryDefence.battleEndSound);
                artilleryDefence.wintitle.setAttribute("style", "display:" + "block");
            }
            
        }
    }
    //console.log(this.sprites.artist.cellIndex);
    this.isAlive = false;

    // animateFromTo and dont loop, the death animation
    if (this.label == "ally") {
        this.sprites.animateFromTo(48, 65, artilleryDefence.canvas, artilleryDefence.context, false);
    } else {
        this.sprites.animateFromTo(66, 83, artilleryDefence.canvas, artilleryDefence.context, false);
    }
}
Soldier.prototype.celebrate = function () {
        this.sprites.animateFromTo(84, 87, artilleryDefence.canvas, artilleryDefence.context, true);
}

// Soldier.prototype = {
//     moveLeft: function () {
//         console.log("is moving left");
//     },
//     moveRight: function () {
//         console.log("is moving right");
//     },
//     setBehaviour: function (behaviour) {
//         this.behaviour = behaviour;
//     },
//     runBehaviour: function () {
//         if (this.behaviour != null) {
//             this.behaviour();
//         } else {
//             console.log(this.getName() + " has no behaviour method.");
//         }
//     }
// }